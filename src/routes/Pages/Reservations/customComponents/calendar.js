import React from 'react';
import PageContainer from '../../../../@jumbo/components/PageComponents/layouts/PageContainer';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';

const today = new Date();
const currentYear = today.getFullYear();
const localizer = momentLocalizer(moment);

const SelectableCalendar = ({events, onSelectUpdateDialog}) => {

  const selectShowDialog = (eventInfo) => {
    onSelectUpdateDialog(eventInfo);
  }

  return (
    <PageContainer>
        <Calendar
          defaultDate={new Date(currentYear, 2, 1)}
          selectable
          localizer={localizer}
          events={events}
          startAccessor="start"
          endAccessor="end"
          style={{ height: 500 }}
          onSelectEvent={eventInfo => selectShowDialog(eventInfo)}
          eventPropGetter={(event) => {
            const backgroundColor = event.eventType == '1' ? '' : '#FC5A5A';
            return { style: { backgroundColor } }
          }}
        />
  )
    </PageContainer>
  );
};

export default SelectableCalendar;