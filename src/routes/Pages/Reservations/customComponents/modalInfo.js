import React, { useEffect } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

// Services.
import { reservations } from '../../../../services/database/reservations';

const useStyles = makeStyles(theme => ({
    formControl: {
      margin: theme.spacing(2),
      minWidth: 120,
      maxWidth: 300,
    }
}));

export default function FormDialogInfo({ openDialog, dialogData, userName, closeDialog, actualizeData }) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState(false);

    useEffect(() => { 
        setOpen(openDialog);
        setData(dialogData);
    }, [openDialog, dialogData]);

    const handleClose = () => {
        setOpen(false);
        closeDialog();
    };

    const deleteReserve = async () => { 
        const { id } = data; 
        const response = await reservations.deleteReservation(id); 
        handleClose();
        actualizeData(response);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Nullify reserve</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {data &&
                            <>
                                <div> User: {userName} </div> 
                                <div> Type reserve: {data.title} </div>
                            </>
                            
                        }
                  
                    </DialogContentText>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    {data.eventType == '2' && 
                        <Button onClick={deleteReserve} color="primary">
                            Delete
                        </Button>
                    }
                    
                </DialogActions>
            </Dialog>
        </div>
    );
}