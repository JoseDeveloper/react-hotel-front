import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from "@material-ui/core/styles";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";

// Dialog.
import FormDialogInfo from "./customComponents/modalInfo";
import  SelectableCalendar  from "./customComponents/calendar";


// Services.
import { reservations } from '../../../services/database/reservations';
import { spaceType } from '../../../services/database/spaceTypes';


const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Reservations', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});


const Reservations = () => {
  const classes = useStyles();
  const { authUser } = useSelector(({ auth }) => auth);

  const [reservationsData, setReservations] = useState([]);
  const [openUpdateDialog, setOpenUpdateDialog] = useState(false);
  const [dialogData, setDialogData] = useState({});
  const [userNameSelected, setUserNameSelected] = useState('');
  
  const [dialogUpdateData, setDialogUpdateData] = useState({});
  

  useEffect(() => {

    async function getReservations() {
      const response = await reservations.getReservations(); 
      console.log('IN COMPONENT',response)
      setReservations(response);
  
    }

    getReservations();
  }, []);

  const handleOpenUpdateDialog = (event) => { 
    setOpenUpdateDialog(true);
    setDialogUpdateData(event);
    setUserNameSelected(event.user.name);
  };

  const handleCloseDialog = () => {
    // setOpenCreateDialog(false);
  };

  const handleCloseUpdateDialog = () => {
    setOpenUpdateDialog(false);
    setDialogData({});
  };

  const actualizeData = (data) => {
    setReservations(data);
    handleCloseDialog();
  }

  return (
    <>
      <PageContainer heading={<IntlMessages id="pages.allReservations" />} breadcrumbs={breadcrumbs}>
        <GridContainer>
          <Grid item xs={12}>
            <Box>
              <IntlMessages id="pages.reservations" />
            </Box>
          </Grid>
          <TableContainer component={Paper}>

            <SelectableCalendar 
              events={reservationsData} 
              onSelectUpdateDialog={handleOpenUpdateDialog} 
            />

          </TableContainer>
        </GridContainer>

      </PageContainer>
      <FormDialogInfo
        openDialog={openUpdateDialog}
        dialogData={dialogUpdateData}
        userName={userNameSelected}
        closeDialog={handleCloseUpdateDialog}
        actualizeData={actualizeData}
      />
    </>
  );
};

export default Reservations;
