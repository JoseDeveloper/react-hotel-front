import React, { useEffect } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

// Services.
import { reservationsUser } from '../../../../services/database/reservationsUser';

const useStyles = makeStyles(theme => ({
    formControl: {
      margin: theme.spacing(2),
      minWidth: 120,
      maxWidth: 300,
    }
}));

export default function FormDialog({ openDialog, dialogData, closeDialog, actualizeData, spaceTypeList }) {
    const classes = useStyles();
    const { authUser } = useSelector(({ auth }) => auth);
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState(false);
    const [reserveData, setReserveData] = React.useState({title: '', spaceTypeSelected: '0', spaceType: {}});

    useEffect(() => { 
        setOpen(openDialog);
        setData(dialogData);
    }, [openDialog, dialogData]);

    const handleClose = () => {
        setOpen(false);
        closeDialog();
    };

    const onSetReserveData = (e) => { 
        const { id, name } = e.target.value; 
        setReserveData({ ...reserveData, spaceType: e.target.value, spaceTypeSelected: id, title: name });
    }

    const send = async () => { 
        const { id: userId } = authUser;
        const { start, end } = data;
        const { spaceTypeSelected, title } = reserveData;
        const objectToDb = { 
            userId,
            title, 
            reservationDate: start, 
            start, 
            end, 
            reserve: title, 
            spaceType: spaceTypeSelected, 
            eventType: '1' 
        };
        
        const response = await reservationsUser.saveUserReservation(objectToDb); 
        actualizeData(response);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Generate reserve</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {/* {data.title} */}
                    </DialogContentText>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="demo-multiple-name-label">Select space type</InputLabel>
                        <Select
                            labelId="demo-multiple-name-label"
                            id="demo-multiple-name"
                            value={reserveData.spaceType}
                            onChange={e => onSetReserveData(e)}
                            input={<Input />}
                        >
                            {spaceTypeList.map((item, i) => (
                                <MenuItem key={i} value={item} name={item.name}>
                                    {item.name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={send} color="primary">
                        Send
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}