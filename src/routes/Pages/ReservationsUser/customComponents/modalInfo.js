import React, { useEffect } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

// Services.
import { reservationsUser } from '../../../../services/database/reservationsUser';

const useStyles = makeStyles(theme => ({
    formControl: {
      margin: theme.spacing(2),
      minWidth: 120,
      maxWidth: 300,
    }
}));

export default function FormDialogInfo({ openDialog, dialogData, closeDialog, actualizeData }) {
    const classes = useStyles();
    const { authUser } = useSelector(({ auth }) => auth);
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState(false);
    const [reserveData, setReserveData] = React.useState({title: '', spaceTypeSelected: '0', spaceType: {}});

    useEffect(() => { console.log('DIALOG DATA', dialogData)
        setOpen(openDialog);
        setData(dialogData);
    }, [openDialog, dialogData]);

    const handleClose = () => {
        setOpen(false);
        closeDialog();
    };

    const nullify = async () => { 
        const { id } = data; 
        const { id: userId } = authUser;
        const objectToDb = { id, eventType: '2', userId };
        
        const response = await reservationsUser.updateUserReservation(objectToDb); 
        handleClose();
        actualizeData(response);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Nullify reserve</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {/* <div>User reserve: {data.user.name} </div> */}
                        <div>Type reserve: {data.title} </div>
                    </DialogContentText>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={nullify} color="primary">
                        Nullify
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}