import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import DoneAllIcon from '@material-ui/icons/DoneAll';

// Dialog.
import FormDialog from "./customComponents/modal";
import FormDialogInfo from "./customComponents/modalInfo";

import  SelectableCalendar  from "./customComponents/calendar";


// Services.
import { reservationsUser } from '../../../services/database/reservationsUser';
import { spaceType } from '../../../services/database/spaceTypes';


const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Reservations', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});


const CreateReservations = () => {
  const classes = useStyles();
  const { authUser } = useSelector(({ auth }) => auth);

  const [reservationsData, setReservations] = useState([]);
  const [openUpdateDialog, setOpenUpdateDialog] = useState(false);
  const [openCreateDialog, setOpenCreateDialog] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [dialogData, setDialogData] = useState({});
  const [dialogUpdateData, setDialogUpdateData] = useState({});
  const [spaceTypeList, setSpaceTypeList] = React.useState([]); 
  

  useEffect(() => {

    async function getReservationsUser() {
      const { id } = authUser;
      const response = await reservationsUser.getReservationsByUser(id); 
      const spaceTypes = await spaceType.getSpaceTypes(); 
     
      setReservations(response);
      setSpaceTypeList(spaceTypes);
    }

    getReservationsUser();
  }, []);

  const handleOpenUpdateDialog = (event) => { 
    setOpenUpdateDialog(true);
    setDialogUpdateData(event);
  };

  const handleOpenCreateDialog = (event, row) => {
    // e.preventDefault();
    setOpenCreateDialog(true);
    setDialogData(event);
  };

  

  const handleCloseDialog = () => {
    setOpenCreateDialog(false);
    setDialogData({});
  };

  const handleCloseUpdateDialog = () => {
    setOpenUpdateDialog(false);
    setDialogData({});
  };

  

  const actualizeData = (data) => {
    setReservations(data);
    setOpenCreateDialog(false);
    handleCloseDialog();
  }

  return (
    <>
      <PageContainer heading={<IntlMessages id="pages.asignReservations" />} breadcrumbs={breadcrumbs}>
        <GridContainer>
          <Grid item xs={12}>
            <Box>
              <IntlMessages id="pages.reservations" />
            </Box>
          </Grid>
          <TableContainer component={Paper}>

            <SelectableCalendar 
              events={reservationsData} 
              onSelectUpdateDialog={handleOpenUpdateDialog} 
              onSelectCreateDialog={handleOpenCreateDialog} 
            />

          </TableContainer>
        </GridContainer>

      </PageContainer>
      <FormDialog
        openDialog={openCreateDialog}
        dialogData={dialogData}
        closeDialog={handleCloseDialog}
        actualizeData={actualizeData}
        spaceTypeList={spaceTypeList}
      />
      <FormDialogInfo
        openDialog={openUpdateDialog}
        dialogData={dialogUpdateData}
        closeDialog={handleCloseUpdateDialog}
        actualizeData={actualizeData}
      />
    </>
  );
};

export default CreateReservations;
