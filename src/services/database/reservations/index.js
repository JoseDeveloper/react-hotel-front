import axios from '../../config';

export const reservations = {
  getReservations: () => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.get(`reservation`, {})
      .then(({ data }) => {
        if (data) {
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },
  deleteReservation: (id) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.delete(`reservation/${id}`, {})
      .then(({ data }) => {
        if (data) {
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },
  

};
