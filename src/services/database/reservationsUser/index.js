import { fetchError, fetchStart, fetchSuccess } from '../../../redux/actions';
import { setAuthUser, updateLoadUser } from '../../../redux/actions/Auth';
import React from 'react';
import axios from '../../config';

export const reservationsUser = {
  getReservationsByUser: (userId) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.get(`reservation/${userId}`, {})
      .then(({ data }) => {
        if (data) {
        
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },

  saveUserReservation: (data) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('reservation', data)
      .then(({ data }) => {
        if (data) {
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },

  updateUserReservation: (data) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.put(`reservation/${data.id}`, data)
      .then(({ data }) => {
        if (data) {
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  }

};

