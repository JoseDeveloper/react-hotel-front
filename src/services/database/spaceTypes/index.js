import axios from '../../config';

export const spaceType = {
  getSpaceTypes: () => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.get('space-type', {})
      .then(({ data }) => {
        if (data) {
     
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  }

};

